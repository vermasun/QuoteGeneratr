package com.amiti;



import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;  
import org.springframework.web.bind.annotation.RequestMapping;  
import org.springframework.web.servlet.ModelAndView;



@Controller  
public class PremiumQuoteGeneratrController { 

    static private final Log log = LogFactory.getLog(PremiumQuoteGeneratrController.class);
    @RequestMapping("/premiumQuoteGeneratr.html")  
    public ModelAndView helloWorld(HttpServletRequest request) throws Exception { 
        log.info("PremiumQuoteGeneratrController started");
        String name=request.getParameter("name");
        String gender=request.getParameter("gender");
        boolean male;
        if(gender.equalsIgnoreCase("male")){
        male=true;
        }else{
            male=false;
        }
        
        int age=Integer.parseInt(request.getParameter("age"));
        boolean hypertension=Boolean.parseBoolean(request.getParameter("hypertension"));
        boolean bloodPressure=Boolean.parseBoolean(request.getParameter("bloodPressure"));
        boolean bloodSugar=Boolean.parseBoolean(request.getParameter("bloodSugar"));
        boolean overweight=Boolean.parseBoolean(request.getParameter("overweight"));
        boolean smoking=Boolean.parseBoolean(request.getParameter("smoking"));
        boolean alcohol=Boolean.parseBoolean(request.getParameter("alcohol"));
        boolean dailyExercise=Boolean.parseBoolean(request.getParameter("dailyExercise"));
        boolean drugs=Boolean.parseBoolean(request.getParameter("drugs"));
    double base=5000;
    int tage=20;
    if(age>18){
        while(true){
            if(tage<=40){
                base=base+base*.1;
            }else{
                base=base+base*.2;;
            }    
            tage=tage+5;
            if(age<tage)
                break;
        }
    }
    if(male)
        base=base+base*.02;
    double health=0;
    if(hypertension)
        health=health+.01;
    if(bloodPressure)
        health=health+.01;
    if(bloodSugar)
        health=health+.01;
    if(overweight)
        health=health+.01;
    base=base+base*health;
    double habits=0;
    if(dailyExercise)
        habits=habits-.03;
    if(smoking)
        habits=habits+.03;
    if(drugs)
        habits=habits+.03;
    if(alcohol)
        habits=habits+.03;
    base=base+base*habits;
        String message = "Health Insurance Premium for Mr. "+name+": Rs. "+Math.round(base);
       log.info("PremiumQuoteGeneratrController End:"+message);
        return new ModelAndView("quote", "message", message);  
    } 
   
}  